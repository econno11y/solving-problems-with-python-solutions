# 1. Your best friend's birthday
class MenuItem:
    def __init__(self, item, calories):
        self.item = item
        self.calories = calories


class Menu:
    def __init__(self, entree, drink, side, dessert):
        self.entree = entree
        self.drink = drink
        self.side = side
        self.dessert = dessert


ENTREE_1 = MenuItem("Hamburger", 522)
DRINK_1 = MenuItem("Diet Soft Drink", 10)
SIDE_1 = MenuItem("French Fries", 130)
DESSERT_1 = MenuItem("Applie pie", 222)

ENTREE_2 = MenuItem("Veggie burger", 399)
DRINK_2 = MenuItem("Coffee", 8)
SIDE_2 = MenuItem("Sweet Potato Fries", 125)
DESSERT_2 = MenuItem("Milkshake", 391)

ENTREE_3 = MenuItem("Impossible burger", 501)
DRINK_3 = MenuItem("Martini", 120)
SIDE_3 = MenuItem("Salad", 72)
DESSERT_3 = MenuItem("Fruit cup", 100)

MEALS = {
    1: Menu(ENTREE_1, DRINK_1, SIDE_1, DESSERT_1),
    2: Menu(ENTREE_2, DRINK_2, SIDE_2, DESSERT_2),
    3: Menu(ENTREE_3, DRINK_3, SIDE_3, DESSERT_3),
}


def calories(entree_num, side_num, dessert_num, drink_num):
    entree_calories = entree_num and MEALS[entree_num].entree.calories
    side_calories = side_num and MEALS[side_num].side.calories
    dessert_calories = dessert_num and MEALS[dessert_num].dessert.calories
    drink_calories = drink_num and MEALS[drink_num].drink.calories

    return entree_calories + side_calories + dessert_calories + drink_calories


# 2. World Ice Skating Day
def skating_day_message(month_num, day_num):
    if month_num < 12 or day_num < 4:
        return "World Ice Skating Day is coming up!"

    if day_num == 4:
        return "YAY! It's World Ice Skating Day!"

    return "You just missed it. There's another next year!"


# 3. Old School Sentiment Analysis
def sentiment(message):
    happy_faces = message.count(":-)")
    sad_faces = message.count(":-(")

    if happy_faces == 0 and sad_faces == 0:
        return "none"

    if happy_faces > sad_faces:
        return "happy"

    if sad_faces > happy_faces:
        return "sad"

    return "unsure"


# 4. Late Night Pizza Craving
def pizza_satisfaction(pizza_size, cheese_multiplier):
    if pizza_size >= 12 and cheese_multiplier == 3:
        return "maximally satisfied"

    if pizza_size >= 10 and cheese_multiplier > 1:
        return "really satisfied"

    return "very satisfied"


# 5. Find the shortest route
def shortest(route_1, route_2, route_3):
    return min(route_1, route_2, route_3)


# 6. Phone scams


def scammer(number):
    digits = [int(num) for num in str(number)]
    first_test = digits[0] == 2 or digits[0] == 4
    second_test = digits[4] == 2 or digits[4] == 4
    third_test = digits[6] == digits[7]
    fourth_test = digits[9] == 0

    if first_test and second_test and third_test and fourth_test:
        return "ignore"
    return "accept"
