# 1. playing the slots
def calculate_plays(num_quarters):
    print(num_quarters)
    times_played_first = 0
    times_played_second = 0
    times_played_third = 0
    slot_machine = "first"

    while num_quarters > 0:
        num_quarters -= 1
        if slot_machine == "first":
            times_played_first += 1
            if times_played_first % 27 == 0:
                num_quarters += 20
            slot_machine = "second"

        elif slot_machine == "second":
            times_played_second += 1
            if times_played_second % 100 == 0:
                num_quarters += 50
            slot_machine = "third"

        else:
            times_played_third += 1
            if times_played_third % 8 == 0:
                num_quarters += 7
            slot_machine = "first"

    return times_played_first + times_played_second + times_played_third


# 2. World's worst TiVo
def calculate_playlist(button_pushes):
    playlists = ["A", "B", "C", "D", "E"]

    for push in button_pushes:
        if push == 1:

            temp = playlists[0]
            playlists[0] = playlists[1]
            playlists[1] = temp
            # playlists[0], playlists[1] = playlists[1], playlists[0] (cool destructuring assignment alternative)

        if push == 2:
            playlists.append(playlists.pop(0))

        if push == 3:
            playlists.insert(0, playlists.pop())

    return playlists


# 3. SAT without studying
# super advanced


def best_student_and_score(answer_key):
    print(len(answer_key))
    azami_score = 0
    baz_score = 0
    caris_score = 0
    azami = ["A", "B", "C"] * (len(answer_key) // 3 + 1)
    baz = ["B", "A", "B", "C"] * (len(answer_key) // 4 + 1)
    caris = ["A", "A", "C", "C", "B", "B"] * (len(answer_key) // 6 + 1)
    for i, c in enumerate(answer_key):
        if azami[i] == c:
            azami_score += 1
        if baz[i] == c:
            baz_score += 1
        if caris[i] == c:
            caris_score += 1
    if azami_score > baz_score and azami_score > caris_score:
        return azami_score, "Azami"
    if baz_score > azami_score and baz_score > caris_score:
        return baz_score, "Baz"
    return caris_score, "Caris"


# 4 Eating Candies


def nia_eat(favorite_color, candy):
    favs = candy.count(favorite_color)
    time = 23 * favs
    if len(candy) > favs:
        time += 17
    return time



# 5 RNA Transcription

MIN_SEQUENCE_LENGTH = 6
COMPLIMENTS = {"A": "T", "T": "A", "G": "C", "C": "G"}
TRANSCRIPTION_KEY = {"A": "U", "T": "A", "G": "C", "C": "G"}


def rna_transcription(dna_strand):

    promoter = dna_strand.find("TATAAT")
    transcription_unit_start = promoter + 10

    # loop through list of possible first sequence starts
    for i in range(transcription_unit_start, len(dna_strand)):

        # loop through list of first possible second sequence ends to end of DNA string
        start = i + 2 * MIN_SEQUENCE_LENGTH - 1  # the place to start the search
        for j in range(start, len(dna_strand)):

            # if substring between i and j is a compliment palindrome,
            # then i is the end of the transcription unit
            if is_compliment_palindrome(dna_strand[i:j]):

                rna_list = [
                    TRANSCRIPTION_KEY[char]
                    for char in dna_strand[transcription_unit_start:i]
                ]
                return "".join(rna_list)


def is_compliment_palindrome(string):
    count = 0
    end = len(string) - 1
    start = 0
    # loop through the string from both sides.
    # If the left side equals the compliment of the right side, increment the count
    # If the count is equal to 5 and you find one more, it's a compliment palindome!
    # If the left side doesn't equal the right side return False
    while start < end:
        compliments = COMPLIMENTS[string[start]] == string[end]
        if compliments and count == 5:
            return True
        elif compliments:
            start += 1
            end -= 1
            count += 1
        else:
            return False
