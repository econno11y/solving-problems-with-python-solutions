# 1. Three Card Monty
POSITIONS = {0: "left", 1: "middle", 2: "right"}
MOVEMENTS = {"L": [0, 1], "O": [0, 2], "R": [1, 2]}


def monty(swaps):
    cards = ["card", "Q", "card"]
    for swap in swaps:
        first_position = MOVEMENTS[swap][0]
        second_position = MOVEMENTS[swap][1]
        temp = cards[first_position]
        cards[first_position] = cards[second_position]
        cards[second_position] = temp

    return POSITIONS[cards.index("Q")]


# 2. Parking lot perfect
def num_same_spaces(yesterday, today):
    result = 0
    for i in range(len(yesterday)):
        if yesterday[i] == "C" and today[i] == "C":
            result += 1
    return result


# 3. Paz's allowance
def cash_on_hand(expenses):
    result = 0
    for expense in expenses:
        result += 30
        result -= expense
    return result + 30


# 4. Strong password
def valid_password(password):
    first_requirement = len(password) > 7 and len(password) <= 12
    number_of_lower_case = sum([char >= "a" and char <= "z" for char in password])
    second_requirement = number_of_lower_case > 1
    number_of_upper_case = sum([char >= "A" and char <= "Z" for char in password])
    third_requirement = number_of_upper_case > 2
    number_of_digits = sum([char >= "0" and char <= "9" for char in password])
    fourth_requirement = number_of_digits > 1

    good = (
        first_requirement
        and second_requirement
        and third_requirement
        and fourth_requirement
    )

    return "good" if good else "bad"


# 5. Secret message


def breakout_room(code, message):
    pure_message = ""
    for char in message:
        if char in code:
            pure_message += char

    return pure_message.count(code)


# 6. Grading Scantrons
def grade_scantron(submission, answer_key):
    result = 0
    for i in range(len(submission)):
        if submission[i] == answer_key[i]:
            result += 1
    return result


# 7. English or German
def language(text):
    ei_sequences = text.count("ei")
    ie_sequences = text.count("ie")

    if ei_sequences > ie_sequences:
        return "German"
    if ie_sequences > ei_sequences:
        return "English"

    return "Maybe French?"


# 8. Flip-flop


def calculate_num_letters(num_pushes):
    num_x = 1
    num_o = 0

    for i in range(1, num_pushes):
        temp = num_x
        num_x = num_o
        num_o = num_o + temp

    return num_x, num_o


# 9. Gold coin
def outcome(starting_player, matches):

    for match in matches:
        if match[1] == starting_player:
            starting_player = match[0]
    return starting_player
