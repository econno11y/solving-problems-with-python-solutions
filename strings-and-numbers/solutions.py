# 1. Strong agreements
def yas(num_a):
    return "Y" + num_a * "a" + "s!"


# 2. A far away galaxy (with list comprehension)
def new_hope(num_fars):
    fars = ["far" for far in range(num_fars)]
    return f"A long time ago in a galaxy {', '.join(fars)} away..."


# 2. A far away galaxy (alternate solution)
def new_hope(num_fars):
    fars = []
    for i in range(num_fars):
        fars.append("far")
    return f"A long time ago in a galaxy {', '.join(fars)} away..."


# 3. The eldest child
def calculate_eldest_age(youngest_age, middle_age):
    return middle_age + middle_age - youngest_age


# 4. Calculate Fahrenheit
def calculate_fahrenheit(celsius):
    return 9 / 5 * celsius + 32

# 5. Making cupcakes
PRICE_OF_FROSTED = 10
PRICE_OF_UNFROSTED = 4
PRICE_OF_TUBE = 1

def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
    # Frosted Cupcakes
    total_frosted = int(num_tubes_frosting / tubes_per_cupcake)
    profit_from_frosted = total_frosted * PRICE_OF_FROSTED

    # Unfrosted Cupcakes
    total_unfrosted = num_cupcakes - total_frosted
    profit_from_unfrosted = total_unfrosted * PRICE_OF_UNFROSTED

    # Leftover Tubes
    leftover_tubes = num_tubes_frosting % tubes_per_cupcake
    profit_from_leftover_tubes = leftover_tubes * PRICE_OF_TUBE

    return profit_from_frosted + profit_from_unfrosted + profit_from_leftover_tubes

# 6. Smaller texts
def count_words(message):
    return False if len(message.split()) > 30 else True
